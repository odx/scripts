#!/bin/sh
# Turns off the system if file creation date is older than 72 hours.
# Appropriate NOPASSWD rules in sudoers are necessary for shutdown to work.
# The script should be set to be invoked by crontab periodically.

CURRENT_DATE=$(date +%s)
CREATION_DATE=$(stat -c %Y ~/.dummy)
POWEROFF_DATE=$((CREATION_DATE+259200))
ALARM_DATE=$((POWEROFF_DATE-7200))

# 259200 - 72 hours
# 7200   - 2 hours

if [ -e $HOME/.dummy ]
then
	if [ $POWEROFF_DATE -lt $CURRENT_DATE ]
	then
		sync;
		setxkbmap -option '';
		echo 0 | sudo tee /proc/sys/kernel/sysrq &> /dev/null;
		sudo shutdown -h -P now &> /dev/null

	elif [ $ALARM_DATE -lt $CURRENT_DATE ]
	then
		aplay $HOME/sounds/alarm.wav &> /dev/null
	fi
fi

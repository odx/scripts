#!/bin/sh

if [ -e /etc/hosts.backup ]
	then
	echo -e "You already made a /etc/hosts.backup file, 
are you trying to append the hosts for the second time? 
Run hosts-unblock, before trying again. Script Aborted."
	else
	sudo cp /etc/hosts /etc/hosts.backup && cat hosts | sudo tee -a /etc/hosts &> /dev/null
fi

#!/bin/sh
# Takes full desktop screenshot and puts it into ~/images/screenshots/desktop/
# Dependencies: maim, xprop.
mkdir -p ~/images/screenshots/desktop &&
maim ~/images/screenshots/desktop/$(date +%Y-%m-%d-%^a-%H:%M).png

#!/bin/sh
# silk-guardian kernel module loader
# prompts for a password graphically (using small urxvt window)
# to run it properly you need to invoke it using this command:
# urxvt -name password-prompt -geometry 33x5 -e "$HOME/gitlab/scripts/shell/silk-module-load.sh"
# and then make sure your WM doesn't tile "password-prompt" window (create a rule if needed)
# Dependencies: alsa (alsa-utils), urxvt, silk (kernel module)

echo -e "\e[?25l"
echo ""
echo "    Type your password to:"
echo ""
echo "    LOAD silk kernel module"
echo -n ""

read -s password
echo $password | sudo -S modprobe silk &>/dev/null
sleep 0.25
CHECK=$(cat /proc/modules | grep -w silk | wc -l)
if [ $CHECK = 1 ]
then
       	aplay -q $HOME/sounds/agent_message.wav &>/dev/null
fi
